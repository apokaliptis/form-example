import { z } from 'zod';


export const FormSchema = z.object({
  FullName: z.string().max(256),
  CompanyName: z.string().max(256),
  Phone: z.string().max(40)
    .regex(
      /[\(]?\d{3}[\)]?[\- ]?\d{3}[\- ]?\d{4}[ ]?([eE][xX][tT][.]?[ ]?\d{1,6}$)?/,
      'Invalid phone number.'
    )
    .optional(),
  Email: z.string().email().max(256),
  Message: z.string().max(100000),
})
  .partial()
  .required({ FullName: true, Message: true })
  .superRefine(({ Email, Phone }, ctx) => {

    // Ensure either phone or email are defined.
    if (!Email && !Phone) {
      return ctx.addIssue({
        path: ['Phone', 'Email'], code: z.ZodIssueCode.custom,
        message: "Either a phone or email is required for us to contact you.",
      })
    }

  })
