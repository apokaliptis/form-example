// Internal
import { fail } from '@sveltejs/kit';
// Vendor
import nodemailer from 'nodemailer';
import { superValidate, message } from 'sveltekit-superforms';
import { zod } from 'sveltekit-superforms/adapters';
// Project
import { FormSchema } from './FormSchema.js';
// Env
import { SMTP_HOST } from '$env/static/private'
import { SMTP_PORT } from '$env/static/private'
import { SMTP_USERNAME } from '$env/static/private'
import { SMTP_PASSWD } from '$env/static/private'
import { FORM_FROM } from '$env/static/private'
import { FORM_TO } from '$env/static/private'

export const config = {
  isr: {
    // Never expires.
    expiration: false,
  }
}

const dateFmt: Intl.DateTimeFormatOptions = { year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric', timeZone: "America/New_York" }

const transporter = nodemailer.createTransport({
  host: SMTP_HOST,
  port: SMTP_PORT,
  secure: Number(SMTP_PORT) === 465,
  auth: {
    user: SMTP_USERNAME,
    pass: SMTP_PASSWD,
  },
})


export async function load() {
  const form = await superValidate(zod(FormSchema));

  return {
    form,
  }
}


export const actions = {
  default: async ({ request }) => {
    const now = new Date()
    const action = "CONTACT_SUBMIT"
    const data = await request.formData()

    const form = await superValidate(data, zod(FormSchema));
    //if (dev)
    console.log('received form submission:', form);

    if (!form.valid) {
      return fail(400, { form, error: "invalid form input" });
    }

    // Send email.
    const m = form.data
    const date = now.toLocaleDateString('en-us', dateFmt)
    const [error, info] = await new Promise<[Error | null, any]>((resolve) => transporter.sendMail({
      from: FORM_FROM,
      to: FORM_TO,
      replyTo: m.Email ? m.Email : undefined,
      subject: `Contact form from ${m.FullName} on ${date}.`,
      html: `<!DOCTYPE html><html lang="en-us"><head><meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>Contact form submission</title>
<style>dl{width:100%;}dl div{display:flex;align-items:flex-start;width:100%;}dt{width:10ch;font-weight:700;margin:0 0 .5em;padding:0}dd{max-width:65ch;margin:0 0 1.5em;padding:0 0 0 1em}</style>
</head><body><dl><div><dt>From:</dt><dd>${m.FullName}</dd></div>
${m.CompanyName ? `<div><dt>Company:</dt><dd>${m.CompanyName}</dd></div>` : ''}
${m.Email ? `<div><dt>Email:</dt><dd>${m.Email}</dd></div>` : ''}
${m.Phone ? `<div><dt>Phone:</dt><dd>${m.Phone}</dd></div>` : ''}
${m.Message ? `<div><dt>Message:</dt><dd>${m.Message}</dd></div>` : ''}
</dl></body></html>`
    }, async (error, info) => resolve([error, info])))


    if (error) {
      console.log(error)
      return fail(502, { form, error: 'mail sending error' })
    }

    //if (dev)
    console.log('Sucessful form submission:', info)

    return message(form, 'Message sent!');
  }
}